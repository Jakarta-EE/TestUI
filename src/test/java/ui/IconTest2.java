package ui;

import de.jensd.fx.glyphs.GlyphIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import de.jensd.fx.glyphs.materialicons.MaterialIcon;
import de.jensd.fx.glyphs.materialicons.MaterialIconView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import ldh.common.testui.component.IconPane;

/**
 * Created by ldh on 2018/4/18.
 */
public class IconTest2 extends Application{

    @Override
    public void start(Stage primaryStage) throws Exception {
        MaterialIconView icon = new MaterialIconView(MaterialIcon.BUG_REPORT);
        Label label = new Label("asdasfasd");
        label.setGraphic(icon);
        icon.setGlyphSize(30);
        icon.setFill(Color.YELLOW);
        Scene scene = new Scene(label, 800, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
